#!/bin/bash
# vim: set noexpandtab:

# Copyright 2021 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

CONSOLESERVER=hbi1.dyn.aikidev.net

if [ -z "$1" ] ; then
	echo $0 needs one or more params:
	./nodes/list_nodes | grep armhf | cut -d '-' -f1
fi

# validate input and modify if needed
for i in "$@" ; do
	case $i in
		bpi0|hb0)	NODE=${i}a	;;
		virt*)		echo "Sorry, I do not know how to handle $i, skipping."
				continue
				;;
		*-*)		NODE=$(echo $i | cut -d '-' -f1) ;;
		*)		NODE=$i		;;
	esac
	ssh $CONSOLESERVER "cd /srv/rb/remote-power ; ./remote-power $NODE ccl"
done

