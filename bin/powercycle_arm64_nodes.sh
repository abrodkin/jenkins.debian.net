#!/bin/bash
# vim: set noexpandtab:

# Copyright 2019 Holger Levsen <holger@layer-acht.org>
#           2020 Mattia Rizzolo <mattia@debian.org>
# released under the GPLv2

JH=reproducible@jumpserv.colo.codethink.co.uk

# validate input
for i in "$@" ; do
	case $i in
		9|10|11|12|13|14|15|16)	: ;;
		*) 	echo 'invalid parameter.'
			exit 1
			;;
	esac
done

# delegate work
# shellcheck disable=2093
exec ssh "$JH" ./sled-power-cycle.sh "$@"


# following is the script that lives at jumpserv.colo.codethink.co.uk
# - note this is not executed due to the `exec` above.
# - remember to place the moonshot-ilo password in the "ilopw" file

for i in "$@" ; do
	case "$i" in
		9|10|11|12|13|14|15|16)	: ;;
		*) continue ;;
	esac
	echo Force power off sled "$i"
	sshpass -f ilopw ssh reproducible@moonshot-ilo "SET NODE POWER OFF FORCE C${i}N1"
	echo sleeping 10 seconds
	sleep 10
	echo Power on sled "$i"
	sshpass -f ilopw ssh reproducible@moonshot-ilo "SET NODE POWER ON C${i}N1"
done
