#!/bin/bash

set -e

WORKSPACE=/srv/janitor/debian-janitor

export SBUILD_CONFIG=/srv/janitor/sbuildrc

export TMPDIR=/srv/janitor/tmp

export PATH=$WORKSPACE/breezy-debian/scripts:$WORKSPACE/debmutate/scripts:$WORKSPACE/lintian-brush/scripts:$WORKSPACE/breezy:$PATH

# Limit memory to 20GB
ulimit -v 20000000

$WORKSPACE/pull_worker.sh \
    --base-url=https://janitor.debian.net/api/ \
    --credentials=/srv/janitor/janitor.creds.json
